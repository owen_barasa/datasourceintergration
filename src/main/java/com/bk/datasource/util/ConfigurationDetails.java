package com.bk.datasource.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ConfigurationDetails {
	INSTANCE;
	Logger logger = LoggerFactory.getLogger(ConfigurationDetails.class);
	private Properties prop = null;
	ConfigurationDetails(){
		prop = new Properties();
		loadproperties();
		
	}
	public Properties loadproperties(){
		//./config/bkdatasource.properties

		InputStream input = null;
		try {
			input = new FileInputStream("config/bkdatasource.properties");
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.info("error loading  config file  system will exit with error message"+ ex.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
		
	}
	
	public Properties getProperties(){
		return prop;
	}

}
