package com.bk.datasource.util;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

public class BKCsvFileWriter<T> {
	Logger logger = LoggerFactory.getLogger(BKCsvFileWriter.class);
	 protected Class<T> Class;
	BKCsvFileWriter(Class<T> hClass){
		Class = hClass;
	}
	
	
	public void writeToFile(Writer writer,String file,List<T> trans) {
		try {
			CustomMappingStrategy <T> strategy =  new CustomMappingStrategy<T>();
	        strategy.setType(Class);
			StatefulBeanToCsv<T> vbeanToCsv = new StatefulBeanToCsvBuilder<T>(writer)
	                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
	               .withMappingStrategy(strategy)
	                .build();
			vbeanToCsv.write(trans);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			logger.error("error writing to the writer "+ e.getMessage());
		}finally{
			if(writer !=null){
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error("error writing to the writer "+ e.getMessage());
				}
			}
			
		}
	}

	public void writeToFile(Writer writer,String file,T trans) {
		try {
			CustomMappingStrategy <T> strategy =  new CustomMappingStrategy<T>();
	        strategy.setType(Class);
			StatefulBeanToCsv<T> vbeanToCsv = new StatefulBeanToCsvBuilder<T>(writer)
	                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
	               .withMappingStrategy(strategy)
	                .build();
			vbeanToCsv.write(trans);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			logger.error("error writing to the writer "+ e.getMessage());
		}finally{
			if(writer !=null){
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error("error writing to the writer "+ e.getMessage());
				}
			}
			
		}
	}

	
	
}
