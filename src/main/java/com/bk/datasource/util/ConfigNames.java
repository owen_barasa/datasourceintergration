package com.bk.datasource.util;

public interface ConfigNames {
 public String OutputfilePathLocation="BK.OUTPUTFILE.DIRECTORY";
 public String cordinatesFle="BK.COORDINATES.FILE";
 public String aggregateFileSummary="BK.AGGREGATE.FILE";
 public String aggregateFilePerCity="BK.AGGREGATE.FILE.PERCITY";
 public String transactionFile="BK.TRANSACTIONS.FILE";
 public String googleKey="BK.GOOGLE.KEY";
 public String customerUrlPath="BK.CUSTOMER.PATH";
 public String transactionUrlPath="BK.TRANSACTION.PATH";
 public String uselocalCopy="BK.USELOCALCUSTOMERCOPYIFEXISTS";
}
