package com.bk.datasource.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.GeoApiContext;

public enum Geolocator {
	INSTANCE;
	Logger logger = LoggerFactory.getLogger(Geolocator.class);
	Map<String, String> mapped = new HashMap<>();

	Geolocator() {
		loadLocationFromFile();
	}

	public GeoApiContext getGeoApiContext() {
		String key = ConfigurationDetails.INSTANCE.getProperties().getProperty("BK.TRANSACTION.PATH");
		GeoApiContext context = new GeoApiContext.Builder().apiKey(key).disableRetries().build();
		return context;
	}

	public String localCopy(double lat, double lng) {
		String key = "" + lat + "" + lng;
		return mapped.get(key);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> loadLocationFromFile() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			logger.info("loading location from file ");
			mapped.putAll(mapper.readValue(new File("config/location.json"), HashMap.class));
			logger.info("loaded location from file " + mapped);
		} catch (IOException e) {
			logger.error("error loading files " + e.getMessage());
		}
		return mapped;
	}

}
