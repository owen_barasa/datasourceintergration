package com.bk.datasource.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bk.datasource.model.Transaction;
import com.bk.datasource.model.TransactionSummarry;
import com.bk.datasource.model.TransactionSummaryCumilative;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public class WriteCsvToResponse {
	private static final Logger logger = LoggerFactory.getLogger(WriteCsvToResponse.class);
	static final String filePath= ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.OutputfilePathLocation);

	public static void writeCities(PrintWriter writer, List<Transaction> transactions) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException  {
    	
    	  BKCsvFileWriter<Transaction> trans = new BKCsvFileWriter<Transaction>(Transaction.class);
    	  trans.writeToFile(writer, null, transactions);
     try {

          String filePath= ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.OutputfilePathLocation);
          String fileName = ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.transactionFile);
  	      String file = filePath+"/"+fileName;
		  validateFilePath(filePath);
		  Writer fwriter =Files.newBufferedWriter(Paths.get(file));
  	      trans.writeToFile(fwriter, file, transactions);
		} catch (IOException e) {
			logger.error("error occured writing to a file "+ e.getMessage());
		}
    
       
    }
    
  
   	public static void writeTransactionSummary(PrintWriter writer, List<TransactionSummarry> transactions) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException  {
    	   BKCsvFileWriter<TransactionSummarry> trans = new BKCsvFileWriter<TransactionSummarry>(TransactionSummarry.class);
           String fileName = ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.aggregateFilePerCity);
           String file = filePath+"/"+fileName;
		try {
			  validateFilePath(filePath);
			  Writer fwriter =Files.newBufferedWriter(Paths.get(file));
	  	      trans.writeToFile(fwriter, file, transactions);
		} catch (IOException e) {
			logger.error("error occured writing to a file "+ e.getMessage());
		}
         
       }

   
   	public static void writeTransactionSummaryCumulative(PrintWriter writer, TransactionSummaryCumilative transactions) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException  {
    	  BKCsvFileWriter<TransactionSummaryCumilative> trans = new BKCsvFileWriter<TransactionSummaryCumilative>(TransactionSummaryCumilative.class);
           String filePath= ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.OutputfilePathLocation);
           String fileName = ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.aggregateFileSummary);
           String file = filePath+"/"+fileName;
	    try {
			validateFilePath(filePath);
			  Writer fwriter =Files.newBufferedWriter(Paths.get(file));
	  	      trans.writeToFile(fwriter, file, transactions);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("error occured writing to a file "+ e.getMessage());
		}   
       }
    
  
    public static void validateFilePath(String filePath) throws IOException{
    	File f = new File(filePath);
    	if(!f.exists()){
			f.mkdir();
     }
    }
}
