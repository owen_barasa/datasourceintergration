package com.bk.datasource.service;

import java.util.List;
import java.util.Map;

import com.bk.datasource.model.Customer;
import com.bk.datasource.model.Transaction;
/**
 * 
 * @author jbarasa
 *
 */
public interface TransactionService {
 

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public Map<String,Customer > getTransactingCustomers() throws  Exception;
	
	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws s 
	 * @throws Exception 
	 */
	public List<Transaction> findAllTransaction() throws  Exception;
	
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public Map<String,String > getAggregate() throws  Exception;

}
