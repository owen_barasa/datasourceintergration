package com.bk.datasource.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvNumber;

public class Transaction {
	
@CsvBindByName (column = "Transaction_Id", required = true)
@CsvBindByPosition(position=0)
private String transactionId;

@CsvBindByName (column = "DateTime", required = true)
@CsvBindByPosition(position=1)
private String dateTime;

@CsvBindByName (column = "Customer_Id", required = true)
@CsvBindByPosition(position=2)
private int customerId;

@CsvBindByName (column = "Customer_Name", required = true)
@CsvBindByPosition(position=3)
private String customerName;

@CsvBindByName (column = "Amount", required = true)
@CsvBindByPosition(position=4)
@CsvNumber("#.###")
private float amount;

@CsvBindByName (column = "CityName", required = true)
@CsvBindByPosition(position = 5)
private String cityName;

public String getTransactionId() {
	return transactionId;
}

public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
}

public String getDateTime() {
	return dateTime;
}

public void setDateTime(String dateTime) {
	this.dateTime = dateTime;
}

public int getCustomerId() {
	return customerId;
}

public void setCustomerId(int customerId) {
	this.customerId = customerId;
}

public String getCustomerName() {
	return customerName;
}

public void setCustomerName(String customerName) {
	this.customerName = customerName;
}

public float getAmount() {
	return amount;
}

public void setAmount(float amount) {
	this.amount = amount;
}

public String getCityName() {
	return cityName;
}

public void setCityName(String cityName) {
	this.cityName = cityName;
}
	
}
