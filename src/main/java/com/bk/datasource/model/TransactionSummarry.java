package com.bk.datasource.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class TransactionSummarry {
	@CsvBindByName (column = "City_Name", required = true)
	@CsvBindByPosition(position=0)
	private String cityName;
	@CsvBindByName (column = "Total_Amount", required = true)
	@CsvBindByPosition(position=1)
	private double totalAmount;
	@CsvBindByName (column = "Unique_Customers", required = true)
	@CsvBindByPosition(position=2)
	private int uniqueCustomer;
	
	@CsvBindByName (column = "Total_Transactions", required = true)
	@CsvBindByPosition(position=3)
	private int totalTransaction;
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getUniqueCustomer() {
		return uniqueCustomer;
	}
	public void setUniqueCustomer(int uniqueCustomer) {
		this.uniqueCustomer = uniqueCustomer;
	}
	public int getTotalTransaction() {
		return totalTransaction;
	}
	public void setTotalTransaction(int totalTransaction) {
		this.totalTransaction = totalTransaction;
	}
	@Override
	public String toString() {
		return "TransactionSummarry [cityName=" + cityName + ", totalAmount=" + totalAmount + ", uniqueCustomer="
				+ uniqueCustomer + ", totalTransaction=" + totalTransaction + "]";
	}
	
	
}
