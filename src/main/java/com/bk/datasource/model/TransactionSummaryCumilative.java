package com.bk.datasource.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class TransactionSummaryCumilative {
	@CsvBindByName (column = "City_Name", required = true)
	@CsvBindByPosition(position=0)
	private int cityName;
	@CsvBindByName (column = "Total_Amount", required = true)
	@CsvBindByPosition(position=1)
	private double totalAmount;
	@CsvBindByName (column = "Unique_Customers", required = true)
	@CsvBindByPosition(position=2)
	private int uniqueCustomer;
	
	@CsvBindByName (column = "Total_Transactions", required = true)
	@CsvBindByPosition(position=3)
	private int totalTransaction;

	public int getCityName() {
		return cityName;
	}

	public void setCityName(int cityName) {
		this.cityName = cityName;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getUniqueCustomer() {
		return uniqueCustomer;
	}

	public void setUniqueCustomer(int uniqueCustomer) {
		this.uniqueCustomer = uniqueCustomer;
	}

	public int getTotalTransaction() {
		return totalTransaction;
	}

	public void setTotalTransaction(int totalTransaction) {
		this.totalTransaction = totalTransaction;
	}
	
	
}
