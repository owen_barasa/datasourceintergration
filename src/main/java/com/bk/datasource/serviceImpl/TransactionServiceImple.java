package com.bk.datasource.serviceImpl;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.bk.datasource.model.Customer;
import com.bk.datasource.model.ResponseRecord;
import com.bk.datasource.model.Transaction;
import com.bk.datasource.service.TransactionService;
import com.bk.datasource.util.ConfigurationDetails;
import com.bk.datasource.util.Geolocator;
import com.bk.datasource.util.ConfigNames;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

public class TransactionServiceImple implements TransactionService{
	
	Logger logger = LoggerFactory.getLogger(TransactionServiceImple.class);
	Map<String,Customer> customers =  new HashMap<>();


	@Override
	public List<Transaction> findAllTransaction() throws Exception {
		String useCusLocalCopy =ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.uselocalCopy);
		if(useCusLocalCopy.equalsIgnoreCase("true") && !customers.isEmpty()){
			logger.info("using local copy of customers ");
		} else{
			 getTransactingCustomers();
		}
		RestTemplate template = new RestTemplate();
		String transactionUrl =ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.transactionUrlPath);
		ResponseEntity<String>  responsee = template.getForEntity(transactionUrl,String.class);
		List<Transaction> transactions = new ArrayList<Transaction>();
		String jsonString=responsee.getBody();
		ObjectMapper mapper = new ObjectMapper();
		List<ResponseRecord> partList = mapper.readValue(jsonString, new TypeReference<List<ResponseRecord>>(){});
	     
		for(ResponseRecord rec:partList ){
				Transaction transaction = new Transaction();
				NumberFormat formatter = new DecimalFormat("#0.00");
				
				Timestamp stamp = rec.getTimestamp();
				Date date = new Date(stamp.getTime());
				SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
				String dateString = format.format(date);
				//Map Customer to transaction 
				Customer customer = customers.get(""+rec.getCustomerId());
				String cityName = getLocation(rec.getLatitude(), rec.getLongitude());				
				//transaction ID concat customer Id with Timestamp
				String transactionId = "BK-C"+rec.getCustomerId()+"T"+rec.getTimestamp().getNanos();
				transaction.setTransactionId(transactionId);
				transaction.setAmount(Float.valueOf(formatter.format(rec.getAmount())));
				transaction.setCustomerId(rec.getCustomerId());
				transaction.setCustomerName(customer.getName());
				transaction.setDateTime(dateString);
				transaction.setCityName(cityName);
				transactions.add(transaction);
		}
		return transactions;
	}

	
	public Map<String,Customer >  getTransactingCustomers() throws  Exception{
		RestTemplate template = new RestTemplate();
		String customersUrl =ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.customerUrlPath);
		ResponseEntity<String>  responsee = template.getForEntity(customersUrl,String.class);
		String jsonString=responsee.getBody();
		XmlMapper xmlMapper = new XmlMapper();
		List<Customer> partList = xmlMapper.readValue(jsonString, new TypeReference<List<Customer>>(){});
		for(Customer customer: partList){
			customers.put(""+customer.getId(), customer);
		}
		return customers;
	}
	
	private String getLocation(double lat,double lng){
		
		String localCopy =Geolocator.INSTANCE.localCopy(lat, lng);
		if(localCopy != null){
			return localCopy;
		}else {
		GeoApiContext context = Geolocator.INSTANCE.getGeoApiContext();
		LatLng location = new LatLng(lat, lng);
		logger.info("calling google to retreive location "+location);
		GeocodingResult[] results;
		try {
			results = GeocodingApi.reverseGeocode(context, location).await();
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			logger.info("data retrieved from google  "+gson.toJson(results[0].addressComponents[0]));
			String cityName =results[0].addressComponents[0].longName;
			return cityName;
		} catch (ApiException | InterruptedException | IOException e) {
			logger.error("error from google server "+ e.getMessage());
		}
		}
		return "N/A";
		
	}


	@Override
	public Map<String, String> getAggregate() throws Exception {
		// TODO Auto-generated method stub
		String useCusLocalCopy =ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.uselocalCopy);
		if(useCusLocalCopy.equalsIgnoreCase("true") && !customers.isEmpty()){
			logger.info("using local copy of customers ");
		} else{
			 getTransactingCustomers();
		}
		String transactionUrl =ConfigurationDetails.INSTANCE.getProperties().getProperty(ConfigNames.transactionUrlPath);
		RestTemplate template = new RestTemplate();
		ResponseEntity<String>  responsee = template.getForEntity(transactionUrl,String.class);

		String jsonString=responsee.getBody();
		ObjectMapper mapper = new ObjectMapper();
		List<ResponseRecord> partList = mapper.readValue(jsonString, new TypeReference<List<ResponseRecord>>(){});
	
	    Map<String,String> mp = new HashMap<>();
		
		return mp;
	}

}
