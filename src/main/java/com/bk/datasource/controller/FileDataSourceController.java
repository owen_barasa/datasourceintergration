package com.bk.datasource.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bk.datasource.model.Transaction;
import com.bk.datasource.model.TransactionSummarry;
import com.bk.datasource.model.TransactionSummaryCumilative;
import com.bk.datasource.serviceImpl.TransactionServiceImple;
import com.bk.datasource.util.WriteCsvToResponse;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

/**
 * 
 * @author jbarasa
 *
 */

@RestController
@RequestMapping(value = "/trans")
public class FileDataSourceController {
	Logger logger = LoggerFactory.getLogger(FileDataSourceController.class);

	DecimalFormat formatter = new DecimalFormat(".##");
	@RequestMapping(value = "/transaction", produces = "text/csv")
	public void findCities(HttpServletResponse response) throws IOException {
		try {
			TransactionServiceImple dataService = new TransactionServiceImple();
			List<Transaction> transactions = dataService.findAllTransaction();
			WriteCsvToResponse.writeCities(response.getWriter(), transactions);
		} catch (CsvDataTypeMismatchException e) {
			logger.error("Error executing service "+ e.getMessage());
		} catch (CsvRequiredFieldEmptyException e) {
			logger.error("Error executing service "+ e.getMessage());
		} catch (Exception e) {
			logger.error("Error executing service "+ e.getMessage());
		}
	}

	

	@RequestMapping(value = "/aggregatePerCity", produces = "text/csv")
	public void findTransactionAggregatePerCity(HttpServletResponse response) {

		try {
			TransactionServiceImple dataService = new TransactionServiceImple();
			List<Transaction> transactions = dataService.findAllTransaction();
			Map<String, List<Transaction>> groupByAlphabet = transactions.stream()
					.collect(Collectors.groupingBy(Transaction::getCityName));
			List<TransactionSummarry> transactionSum = new ArrayList<>();
			groupByAlphabet.keySet().stream().forEach(t -> {
				TransactionSummarry tra = new TransactionSummarry();
				tra.setCityName(t);
				List<Transaction> transact = groupByAlphabet.get(t);
				tra.setTotalTransaction(transact.size());
				tra.setUniqueCustomer(
						transact.stream().collect(Collectors.groupingBy(e -> e.getCustomerId())).keySet().size());
				formatter.setMaximumIntegerDigits(8);
				String formatted = formatter
						.format(transact.stream().collect(Collectors.summingDouble(e -> e.getAmount())));
				tra.setTotalAmount(Double.valueOf(formatted));
				transactionSum.add(tra);
			});
			WriteCsvToResponse.writeTransactionSummary(response.getWriter(), transactionSum);
		} catch (Exception e) {
			logger.error("Error executing service "+ e.getMessage());
		}

	}

	@RequestMapping(value = "/aggregateSummary", produces = "text/csv")
	public void findTransactionSummary(HttpServletResponse response) {
		try {
			TransactionServiceImple dataService = new TransactionServiceImple();
			List<Transaction> transactions = dataService.findAllTransaction();
			Map<String, List<Transaction>> groupByAlphabet = transactions.stream()
					.collect(Collectors.groupingBy(Transaction::getCityName));
			TransactionSummaryCumilative tsc = new TransactionSummaryCumilative();
			tsc.setCityName(groupByAlphabet.size());
			tsc.setTotalTransaction(transactions.size());
			formatter.setMaximumIntegerDigits(8);
			String formatted = formatter
					.format(transactions.stream().collect(Collectors.summingDouble(e -> e.getAmount())));
			tsc.setTotalAmount(Double.valueOf(formatted));
			tsc.setUniqueCustomer(
					transactions.stream().collect(Collectors.groupingBy(e -> e.getCustomerId())).keySet().size());
			WriteCsvToResponse.writeTransactionSummaryCumulative(response.getWriter(), tsc);
		} catch (Exception e) {
			logger.error("error while consuming the service " + e.getMessage());
		}

	}
}
